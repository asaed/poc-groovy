package com.asaed.poc.groovy.stubbingBlog

/**
 * Created by akrem on 7/22/2014.
 */
class StudentScoreService {

    private StudentRepository studentRepository

    String getStudentStatus(studentId) {
        studentRepository = new StudentRepository()
        def student = studentRepository.findStudent(studentId)
        if (student == null) {
           return "StudentId#$studentId was not found"
        }

        def score = student.score
        def status = score > 200 ? "PASS" : "FAIL";
        return "$student.fullName: $status"
    }
}

class StudentRepository {
    Student findStudent(studentId) {
        throw new UnsupportedOperationException()
    }
}

class Student {
    int studentId
    String fullName
    double score
    List<String> courses
}
