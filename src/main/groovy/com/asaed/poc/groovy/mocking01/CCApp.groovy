package com.asaed.poc.groovy.mocking01

/**
 * Created by akrem on 7/18/2014.
 */
class CCApp {
    def isApproved(user) {
        CreditHistory creditHistory = new CreditHistory()
        def score = creditHistory.getCreditScore(user.ssn)
        def numberOfBadAccounts = creditHistory.getBadAccounts(user.ssn).size()
        if (numberOfBadAccounts > 1) return false
        if (score > 600)
            return true
        return false
    }
}

class CreditHistory {
    def getCreditScore(int ssn) {
        //some expensive code
    }

    def getBadAccounts(int ssn) {
        //some expensive code
    }
}


