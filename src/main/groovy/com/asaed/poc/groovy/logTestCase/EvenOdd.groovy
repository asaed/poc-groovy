package com.asaed.poc.groovy.logTestCase

import java.util.logging.Logger

/**
 * Created by akrem on 7/19/2014.
 */
class EvenOdd {
    static final LOGGER = Logger.getLogger('EvenOdd')

    def isEven(number) {
        if (number % 2 == 0) {
            LOGGER.finer "$number is even"
            return true
        }
        LOGGER.finer "$number is odd"
        return false
    }
}
