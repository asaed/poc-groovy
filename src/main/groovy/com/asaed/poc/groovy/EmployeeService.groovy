package com.asaed.poc.groovy

/**
 * Created by akrem on 7/18/2014.
 */
class EmployeeService {
    Object getEmployees() {
        return ['akrem', 'john'];
    }

    def getEmployee(Integer employeeId) {
        if (employeeId == null) {
            throw new IllegalArgumentException("employeeId cannot be null");
        }
    }
}
