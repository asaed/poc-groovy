package com.asaed.poc.groovy.templating02

import groovy.text.GStringTemplateEngine


class ExternalTemplating {

    String getEmailContent(Map binding) {
        //Path to your template file
//        def f = new File('registration.template')
        def f= this.getClass().getResource( '/registration.template' ).text
        def engine = new GStringTemplateEngine()
        try {
            def template = engine.createTemplate(f).make(binding)
            return template.toString()
        }
        catch (Exception e) {
            println "Can't find $f.absolutePath"
        }

    }
}
