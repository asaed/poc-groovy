package com.asaed.poc.groovy.templating01

import groovy.text.SimpleTemplateEngine

class SimpleTemplating {

    String getEmailContent(Map binding) {
        def text = '''
From: $fromEmail
To: $toEmail
Dear $name,
Please click on the following URL to activate your account:
${link}
Thanks,
$signature
<%= new java.text.SimpleDateFormat("MM\\\\dd\\\\yyyy").format(new Date()) %>
'''

        def engine = new SimpleTemplateEngine()
        def template = engine.createTemplate(text).make(binding)
        return template.toString()
    }
}
