package com.asaed.poc.groovy.stubbing01

/**
 * Created by akrem on 7/18/2014.
 */
class CCApp {
    def isApproved(user) {
        def score = (new CreditHistory()).getCreditScore(user.ssn)
        if (score > 600)
            return true
        return false
    }
}

class CreditHistory {
    def getCreditScore(int SSN) {
        // not yet implemented
    }
}


