package com.asaed.poc.groovy

/**
 * Created by akrem on 7/18/2014.
 */
class EmployeeServiceTest extends GroovyTestCase {
    private EmployeeService service = new EmployeeService()

    void testGetEmployeesShouldReturnList() {
        def employees = service.getEmployees();
        assertEquals(['akrem', 'john'], employees);
    }

    void testGetEmployeeShouldThrowExceptionIfIdIsNull() {
        shouldFail(IllegalArgumentException) {
            service.getEmployee(null);
        }
    }
}
