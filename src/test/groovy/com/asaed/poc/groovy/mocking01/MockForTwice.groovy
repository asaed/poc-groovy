package com.asaed.poc.groovy.mocking01

import com.asaed.poc.groovy.stubbing02.CCApp
import com.asaed.poc.groovy.stubbing02.CreditHistory
import groovy.mock.interceptor.MockFor
import junit.framework.AssertionFailedError

/**
 * Created by akrem on 7/18/2014.
 */
class MockForTwice extends GroovyTestCase {


    void testIsApproved_CorrectOrderOfMocks() {
        //with mock order of demand is important
        def creditHistoryMock = new MockFor(CreditHistory)
        //getCreditScore can be called at most once
        creditHistoryMock.demand.getCreditScore { ssn ->
            if (ssn == 123) return 400
            if (ssn == 12) return 700
            return 500
        }
        //getBadAccounts can be called at most once
        creditHistoryMock.demand.getBadAccounts { ssn ->
            return [1, 2]
        }


        def user1 = [ssn: 123]

        def app = new CCApp()
        creditHistoryMock.use {
            assert !app.isApproved(user1)
        }
    }

    void testIsApproved_TooManyCallsWillFail() {
        //with mock order of demand is important
        def creditHistoryMock = new MockFor(CreditHistory)
        //getCreditScore can be called at most once
        creditHistoryMock.demand.getCreditScore { ssn ->
            if (ssn == 123) return 400
            if (ssn == 12) return 700
            return 500
        }
        //getBadAccounts can be called at most once
        creditHistoryMock.demand.getBadAccounts { ssn ->
            return [1, 2]
        }


        def user1 = [ssn: 123]
        def user2 = [ssn: 12]

        def app = new CCApp()
        creditHistoryMock.use {
            assert !app.isApproved(user1)
            def errorMsg = shouldFail(AssertionFailedError) {
                app.isApproved(user2)
            }
            assert errorMsg == "No more calls to 'getCreditScore' expected at this point. End of demands."
        }
    }
}
