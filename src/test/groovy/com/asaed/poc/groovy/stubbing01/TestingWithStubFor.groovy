package com.asaed.poc.groovy.stubbing01

import groovy.mock.interceptor.StubFor

/**
 * Created by akrem on 7/18/2014.
 */
class TestingWithStubFor extends GroovyTestCase {

    void testIsApproved() {
        //stub collaborator
        def creditHistoryStub = new StubFor(CreditHistory)
        creditHistoryStub.demand.getCreditScore { ssn ->
            if (ssn == 123) return 400
            if (ssn == 12) return 700
            return 500
        }

        //simple duck-typing
        def user1 = [ssn: 123]
        def app = new CCApp()

        creditHistoryStub.use {
            assert !app.isApproved(user1)
        }
    }
}
