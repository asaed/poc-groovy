package com.asaed.poc.groovy.templating02


class ExternalTemplatingTest extends GroovyTestCase {
    void testGetEmailContent() {
//        URI filePath = this.class.getResource('/registration.template').toURI()
//        println filePath
        def templating = new ExternalTemplating()
        def binding = ["fromEmail": "registration@groovy.codehaus.org",
                       "toEmail"  : "john@smith.com", "name": "John Smith",
                       "link"     : "http://groovy.codehaus.org/activate", "signature": "Registration"]

        def emailBody = templating.getEmailContent(binding);

        println emailBody
    }
}
