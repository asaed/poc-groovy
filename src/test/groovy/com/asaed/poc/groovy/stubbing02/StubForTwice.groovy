package com.asaed.poc.groovy.stubbing02

import groovy.mock.interceptor.StubFor

/**
 * Created by akrem on 7/18/2014.
 */
class StubForTwice extends GroovyTestCase {

    void testIsApproved() {
        def creditHistoryStub = new StubFor(CreditHistory)
        //getBadAccounts can be called at most twice
        creditHistoryStub.demand.getBadAccounts(1..2) { ssn ->
            if (ssn == 123) return ["Account 1", "Account 2"]
            if (ssn == 12) return ["Account 1"]
            return [1, 2]
        }
        //getCreditScore can be called at most twice
        creditHistoryStub.demand.getCreditScore(1..2) { ssn ->
            if (ssn == 123) return 400
            if (ssn == 12) return 700
            return 500
        }


        def user1 = [ssn: 123]
        def user2 = [ssn: 12]

        def app = new CCApp()
        creditHistoryStub.use {
            assert !app.isApproved(user1)
            assert app.isApproved(user2)
        }
    }
}
