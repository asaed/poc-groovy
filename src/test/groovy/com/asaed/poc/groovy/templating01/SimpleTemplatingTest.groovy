package com.asaed.poc.groovy.templating01


class SimpleTemplatingTest extends GroovyTestCase {

    void testReturnEmailContent(){
        def templating = new SimpleTemplating();
        def binding = ["fromEmail": "registration@groovy.codehaus.org",
                       "toEmail"  : "john@smith.com", "name": "John Smith",
                       "link"     : "http://groovy.codehaus.org/activate", "signature": "Registration"]

        def emailBody = templating.getEmailContent(binding);

        println emailBody
    }
}