package com.asaed.poc.groovy.stubbingBlog

import groovy.mock.interceptor.StubFor

class StudentScoreServiceTest1 extends GroovyTestCase {

    private StudentScoreService service
    StubFor repoStub

    @Override
    void setUp() {
        repoStub = new StubFor(StudentRepository)
        repoStub.demand.findStudent { studentId ->
            return null
        }
        service = new StudentScoreService()
    }


    void testNonExistingStudentStatus() {
        //To use the stub, let's wrap our test with repoStub.use
        repoStub.use {
            assertEquals("StudentId#1 was not found", service.getStudentStatus(1))
        }
    }

    //PASS
    void test_noVerify_FindStudentNeverCalled() {
        repoStub.use {
        }
    }

    //PASS
    void test_noVerify_FindStudentCalledOnce() {
        repoStub.use {
            assertEquals("StudentId#1 was not found", service.getStudentStatus(1))
        }
    }

    //FAIL with "No more calls to 'findStudent' expected at this point. End of demands."
    void test_noVerify_FindStudentCalledMoreThanOnce() {
        shouldFail() {
            repoStub.use {
                assertEquals("StudentId#1 was not found", service.getStudentStatus(1))
                assertEquals("StudentId#2 was not found", service.getStudentStatus(2))
            }
        }
    }

    //FAIL with "expected 1..1 call(s) to 'findStudent' but was called 0 time(s)"
    void test_withVerify_FindStudentNeverCalled() {
        shouldFail() {
            repoStub.use {
            }
            repoStub.verify()
        }
    }

    //PASS
    void test_withVerify_FindStudentCalledOnce() {
        repoStub.use {
            assertEquals("StudentId#1 was not found", service.getStudentStatus(1))
        }
        repoStub.verify()
    }

    //FAIL with "No more calls to 'findStudent' expected at this point. End of demands."
    void test_withVerify_FindStudentCalledMoreThanOnce() {
        shouldFail() {
            repoStub.use {
                assertEquals("StudentId#1 was not found", service.getStudentStatus(1))
                assertEquals("StudentId#2 was not found", service.getStudentStatus(2))
            }
            repoStub.verify()
        }
    }

}
