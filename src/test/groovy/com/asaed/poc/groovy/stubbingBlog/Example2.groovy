package com.asaed.poc.groovy.stubbingBlog

import groovy.mock.interceptor.StubFor

class StudentScoreServiceTest2 extends GroovyTestCase {

    private StudentScoreService service
    StubFor repoStub

    @Override
    void setUp() {
        repoStub = new StubFor(StudentRepository)
        repoStub.demand.findStudent { studentId ->
            if (studentId == 45)
                return [studentId: 45, fullName: "Steven Tyler", score:  201]
            else
                return null
        }

        service = new StudentScoreService()
    }

    void testExistingStudentStatus() {
        repoStub.use {
            assertEquals("Steven Tyler: PASS", service.getStudentStatus(45))
        }
    }

    void testNonExistingStudentStatus() {
        repoStub.use {
            assertEquals("StudentId#1 was not found", service.getStudentStatus(1))
        }
    }
}
