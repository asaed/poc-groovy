package com.asaed.poc.groovy.logTestCase

import java.util.logging.Level

/**
 * Created by akrem on 7/19/2014.
 */
class EvenOddTest extends GroovyLogTestCase {

    private numbers
    private evenOdd

    @Override
    void setUp() {
        evenOdd = new EvenOdd()
        numbers = [2, 3, 4, 5, 6, 7, 8]
    }

    void testEvenOddLog() {
        def log = stringLog(Level.FINER, 'EvenOdd') {
            numbers.each { evenOdd.isEven(it) }
        }
//        println log
        assert log.contains("2 is even")
        assert log.contains("3 is odd")
        assert log.contains("4 is even")
        assert log.contains("5 is odd")
        assert log.contains("6 is even")
        assert log.contains("7 is odd")
        assert log.contains("8 is even")
        assert !log.contains("9 is odd")

    }
}
