package com.asaed.poc.groovy.testing

/**
 * Created by akrem on 7/18/2014
 * Borrowed from Groovy Grails Recipes book by Bashar Abdul-Jawad
 */
class TestingWithMapsAndDuckTyping extends GroovyTestCase {

    void testIsApprovedShouldCheckCreditScoreOfUser() {
        //a mock for CreditHistory using duck-typing and maps
        def creditHistory = [getCreditScore: { ssn ->
            if (ssn == 123)
                return 400
            if (ssn == 12)
                return 700
            return 500
        }]

        //User class is not implemented, but for testing we will duck-type some instances
        def user1 = [ssn: 123]
        def user2 = [ssn: 12]
        def user3 = [ssn: 1]

        def app = new CCApp()
        assert app.isApproved(creditHistory, user1) == false
        assert app.isApproved(creditHistory, user2) == true
        assert app.isApproved(creditHistory, user3) == false
    }

}
